<?php

// tells browser this is a json content and allow cross domain request
function setHeader() {
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Credentials: true");
    header("Access-Control-Allow-Origin: http://khanhthanh.lan");
    header("Access-Control-Allow-Origin: http://192.168.1.1");
}

// insert error message to obj and print in json format then exit.
function exitOnFailure($obj, $message) {
    $obj->message = $message;
    exit(json_encode($obj));
}

// build URL based on day of the month and weekday.
function generateURL() {
    $rNum = rand(date("N"), date("jN"));
    return 'https://sose.xyz/page/' . $rNum;
}

// get all image links from sose source using regex
function getImageLink($source) {
    // use regex to find all tumblr links
    $re = '/data-photo-high="(.*?)"/m';

    // regex use PREG_PATTERN_ORDER => $matches[1] is an array contains matched strings
    $nMatches = preg_match_all($re, $source, $matches, PREG_PATTERN_ORDER, 0);

    // return an array with message error on failure.
    if (!$nMatches) {
        return array(false, "Cannot get image link");
    }
    
    //return an array of image links
    return array(true, $matches[1]);
}

function main() {
    // set header
    setHeader();
    
    // define return variable
    $result->sucess = false;
    
    // get sose link with random page
    $url = generateURL();
    
    // get the site source
    $source = file_get_contents($url);

    // exit if an error occurs (connection error or unreachable)
    if ($source == false) {
        exitOnFailure($result, "Cannot connect to sose.xyz");
    }
    // get image links
    $links = getImageLink($source);

    // exit if cannot get those image links
    if (!$links[0]) {
        exitOnFailure($result, links[1]);
    }
    
    // build var in json format contains image links
    $result->success = true;
    $result->total = count($links[1]);
    $result->large = $links[1];
    $result->small = str_replace('1280.jpg', '100.jpg', $links[1]);
    
    // get callback function name
    $callbackFunc = isset($_GET['callback']) ? $_GET['callback'] : '';
    
    // print result in json, wrapped by callback function
    if ($callbackFunc) {
        echo $callbackFunc.'('.json_encode($result).');';
    } else {
        echo json_encode($result);
    }
}

main();

?>